#pragma once

#include "common.h"

Color antyAliasing(Color& pixelColor, int samplesPerPixel) {

	auto scale = 1.0 / samplesPerPixel;

	//gamma corection
	pixelColor[0] = std::sqrt(scale * pixelColor[0]);
	pixelColor[1] = std::sqrt(scale * pixelColor[1]);
	pixelColor[2] = std::sqrt(scale * pixelColor[2]);

	return Color{
		256 * std::clamp<T>(pixelColor[0], 0, 0.999),
		256 * std::clamp<T>(pixelColor[1], 0, 0.999),
		256 * std::clamp<T>(pixelColor[2], 0, 0.999)
	};
}