#pragma once

#include "Hittable.h"

class Sphere : public Hittable
{
public:
	Point3 center;
	float radius;
	float radiusSquare;


	Sphere(const Point3 center, const T radius) :center(center), radius(radius), radiusSquare(radius* radius) {}

	virtual bool hit(const Ray& r, T tMin, T tMax, HitRecord& rec) const 
	{
		Vec3 oc = r.origin - center;
		auto a = r.dir.length_squared();
		auto halfB = oc.dot(r.dir);
		auto c = oc.length_squared() - radiusSquare;
		auto discriminant = halfB*halfB - a*c;
		
		if (discriminant < 0)
			return false;

		auto sqrtd = std::sqrt(discriminant);

		auto root = (-halfB - sqrtd) / a;
		if (root < tMin || tMax < root) {
			root = (-halfB + sqrtd) / a;

			if (root < tMin || tMax < root)
				return false;
		}

		rec.t = root;
		rec.p = r.at(rec.t);
		Vec3 outwardNormal = (rec.p - center) / radius;
		rec.setFaceNormal(r, outwardNormal);

		return true;
	}

};
