#pragma once

#include "common.h"


class Camera {
public:
	Camera() {
		T ratio = static_cast<T>(width) / static_cast<T>(height);
		T viewportHeight = 2.0;
		T viewportWidth = viewportHeight * ratio;
		T focal = 1.0;

		origin = Point3{ 0, 0, 0 };
		horizontal = Vec3{ viewportWidth, 0, 0 };
		vertical = Vec3{ 0.0, viewportHeight, 0 };
		lowLeftCorner = origin - horizontal/2 - vertical/2 - Vec3(0, 0, focal);
	}

	Ray getRay(T u, T v) const
	{
		return Ray(origin, lowLeftCorner + u*horizontal + v*vertical - origin);
	}

private:
	Point3 origin;
	Point3 lowLeftCorner;
	Vec3 horizontal;
	Vec3 vertical;

};