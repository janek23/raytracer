#pragma once

#include <opencv2/core/core.hpp>

#include "common.h"

using T = float; //precision type 

using cvVec = cv::Vec<T, 3>;

template<typename Type = T> Type random(T min, T max);
template<typename Type = T> Type random();

class Vec3 : public cvVec
{
public:
	using cvVec::cvVec; //inherit constructors

	Vec3(Vec3 const& v) : cvVec(v) {};
	Vec3(cvVec const& v) :cvVec(v) {};

	inline T length_squared() const {
		auto& el = *this;
		return el[0] * el[0] + el[1] * el[1] + el[2] * el[2];
	}

	inline T lenght() const {
		if constexpr (std::is_same<T, float>::value)
			return std::sqrtf(length_squared());
		else
			return std::sqrt(length_squared());
	}

	inline Vec3 unitVector() const {
		return (*this) / this->lenght();
	}

	inline static Vec3 randomVec() {
		return Vec3(random<T>(), random<T>(), random<T>());
	}

	inline static Vec3 randomVec(T min, T max) {
		return Vec3(random<T>(min, max), random<T>(min, max), random<T>(min, max));
	}

	inline static Vec3 randomInUnitSphere() {
		while (true) {
			auto vec = randomVec(-1, 1);
			if (vec.length_squared() >= 1)
				continue;

			return vec;
		}
	}



};

using Point3 = Vec3;
using Color = Vec3;