#pragma once

#include <opencv2/core/core.hpp>
#include "Vec3.h"

class Ray {
public:
	Point3 origin;
	Vec3 dir;

	Ray() {};
	Ray(const Point3& originPoint, const Vec3& direction) :origin(originPoint), dir(direction) {}

	Vec3 at(float t) const { return origin + (t * dir); }
};