#pragma once

#include "Ray.h"

struct HitRecord {
	Point3 p;
	Vec3 normal;
	T t;
	bool frontFace;

	inline void setFaceNormal(const Ray& r, const Vec3& outwardNormal) {
		frontFace = r.dir.dot(outwardNormal) < 0;
		if (frontFace)
			normal = outwardNormal;
		else
			normal = -outwardNormal;
	}
};

class Hittable {
public:
	virtual bool hit(const Ray& r, T tMin, T tMax, HitRecord& rec) const = 0;
};
