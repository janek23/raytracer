#pragma once

#include <limits>
#include <cmath>
#include <numbers>
#include <random>
#include <algorithm>

#include "Ray.h"
#include "Color.h"


	//settings 
	constexpr bool measureTime = true;
	constexpr int width = 1920;
	constexpr int height = 1080;
	constexpr int samplesPerPixel = 50;
	constexpr int maxDepth = 20;

	constexpr T infinity = std::numeric_limits<T>::infinity();
	constexpr T pi = std::numbers::pi_v<T>;

	inline T degreesToRad(T degrees) {
		return degrees * pi / T{ 180 };
	}

	template<typename Type = T>
	inline Type random(T min, T max) {
		static std::uniform_real_distribution<Type> distribution(min, max);
		static std::mt19937 generator;
		return distribution(generator);
	}

	template<typename Type = T>
	inline Type random() {
		return random(0.0, 1.0);
	}


