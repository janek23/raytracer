#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <chrono>
#include <memory>

#include "common.h"
#include "Sphere.h"
#include "Hittable.h"
#include "Camera.h"

using hittableList = std::list<std::shared_ptr<Hittable>>;

bool hit(const hittableList& objList, const Ray& r, T tMin, T tMax, HitRecord& rec) {
	HitRecord tempRec;
	auto hitAnything = false;
	auto closestSoFar = tMax;

	for (const auto& obj : objList) {
		if (obj->hit(r, tMin, closestSoFar, tempRec)) {
			hitAnything = true;
			closestSoFar = tempRec.t;
			rec = tempRec;
		}
	}
	return hitAnything;
}

Color rayColour(const Ray& r, const hittableList& objList, int depth )
{
	HitRecord rec;

	//ray bounce limit. return black.
	if (depth <= 0)
		return Color(0, 0, 0);


	if (hit(objList, r, 0.001, infinity, rec)) {
		Point3 target = rec.p + rec.normal + Vec3::randomInUnitSphere();
		return 0.5 * rayColour(Ray(rec.p, target - rec.p), objList, depth -1);
	}
	Vec3 unitDirection = r.dir.unitVector();
	auto t = 0.5 * (unitDirection[1] + 1);

	return Color{ 0.5,0.5,0.5 };//(1.0 - t) * Color(1.0, 1.0, 1.0) + t * Color(0.5, 0.7, 1.0);
}

void render(cv::Mat& buffer, const hittableList& objList, const Camera& cam, int& duration) 
{
	auto t1 = std::chrono::high_resolution_clock::now();

	for (size_t r = 0; r < buffer.rows; r++) {
		for (size_t c = 0; c < buffer.cols; c++) {
			Color pixelColor{ 0, 0, 0 };
			for (int s = 0; s < samplesPerPixel; ++s) 
			{
				auto u = (static_cast<T>(c) + random<T>()) / (buffer.cols - 1);
				auto v = (static_cast<T>(r) + random<T>()) / (buffer.rows - 1);

				Ray ray = cam.getRay(u, v);
				pixelColor += rayColour(ray, objList, maxDepth);
			}
			auto aliasedColor = antyAliasing(pixelColor, samplesPerPixel);
			buffer.at<cv::Vec3b>(r, c) = aliasedColor;
		}
	}
	
	auto t2 = std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
}

int main()
{
	Camera cam;

	hittableList objList{
		std::make_shared<Sphere>(Point3(0, 0, -1), 0.5),
		std::make_shared<Sphere>(Point3(0, 100.5, -1), 100)
	};
	
	cv::Mat buffer(height, width, CV_8UC3);

	int durationMs;

	render(buffer, objList, cam, durationMs);

	//draw excution time to buffer
	cv::putText(buffer, std::to_string(durationMs) + "ms", cv::Point(buffer.cols * 0.8, buffer.rows * 0.05), 1, 2,cv::Scalar(0, 0, 0));

	cv::waitKey(0);
	return 0;
}
